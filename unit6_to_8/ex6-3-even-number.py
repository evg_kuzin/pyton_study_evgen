#! /usr/bin/env/ python

# Asks for a number.
# Prints if it is even or odd
number = float(input("Tell me a number: ")) 
if number % 2 == 0:
	print ("The number", int(number), "is even")
elif number % 2 == 1:
	print ("The number", int(number), "is odd")
else :
	print ("The number", number, "is very strange")