#! /usr/bin/env/ python

# Plays the guessing game higher or lower
# This should actually be something that is semi random like the
# last digits of the time or something else, but that will have to # wait till a later chapter. (Extra Credit, modify it to be random # after the Modules chapter)
number = 7
guess = -1
print("Guess the number!") 

while guess != number:
	guess =int(input("Guess the number..."))
	if guess > number: 
		print ("Guess the lower number...")
	elif guess < number:
		print ("Guess the greater number...")
	elif guess == number:
		 print ("Hurray!!...")
