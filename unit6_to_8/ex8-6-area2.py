#! /usr/bin/python
#-*-coding: utf-8 -*-
# calculates a given rectangle area
def hello(): 
	print('Hello!')

def print_options():
	print ("Options: ")
	print (" 'p' print options")
	print (" 's' calculate area of square")
	print (" 'r' calculate area of rectangle")
	print (" 'c' calculate area of circle")	
	print (" 'q' quit the program")
	print()

def square(width, height): 
	return width * height

def print_welcome(name): 
	print('Welcome,', name)

def positive_input(prompt): 
	number = float(input(prompt)) 
	while number <= 0:
		print('Must be a positive number')
		number = float(input(prompt)) 
	return number

name = input('Your Name: ') 
hello()
print_welcome(name)
print()
print("This program calculates the square of a figure, depending on it's form") 
#print('enter the width and height below.') 
print()

inp = "p"
while  inp != "q":
	if inp == "p":
		print_options()
		inp = input("What are you going to calculate? ")
	elif inp == "s":
		w = positive_input('Width: ')
		h = positive_input('Height: ')
		if w != h:
			print("This is not a square")
		else:	
			print("Square Area is: ", square(w,h))
		print_options()
		inp = input("What are you going to calculate? ")	
	elif inp == "r":
		w = positive_input('Width: ')
		h = positive_input('Height: ')	
		if w==h:
			print ("This is a square, so select proper option to calculate square of a square")
		else:
			print("Rectangle Area is: ", square(w,h))
		print_options()
		inp = input("What are you going to calculate? ")	
	elif inp == "c":
		rad = positive_input("Radius: ")
		print("Circle Area is: ", 3.14*rad*rad)
		print_options()
		inp = input("What are you going to calculate? ")	

# print('Width =', w, ' Height =', h, ' so Area =', area(w, h))