#! /usr/pin/python

def print_options():
	print ("Options: ")
	print (" 'p' print options")
	print (" 'c' convert from Celsius")
	print (" 'f' convert from Farenheit")
	print (" 'q' quit the program")

def celsius_to_farenheit(cels):
	return 9.0/5.0*cels+32

def farenheit_to_celsius(faren):
	return 5.0/9.0*(faren-32.0)

inp = "p"
while inp != "q":
	if inp == "c":
		temp = float(input("Enter Celsius degrees: "))
		print("cels_to_far:  ",celsius_to_farenheit(temp))
		inp = input("Choose an option: ")
	elif inp == "f":
		temp = float(input("Enter Celsius degrees: "))
		print("far_to_cels:  ",farenheit_to_celsius(temp))
		inp = input("Choose an option: ")		
	elif inp == "p":
		print_options()
		inp = input("Choose an option: ")

