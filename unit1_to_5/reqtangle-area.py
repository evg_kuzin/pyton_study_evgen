#! /usr/etc/env/ python

# This program calculates the perimeter and area of a rectangle
print("Calculate information about a rectangle") 
length = float(input("Length (mm): "))
width = float(input("Width (mm): "))
print ("Area of the rectangle is: ", length * width, "mm")
print ("Perimeter of the rectangle is: ", length * 2 + width * 2, "mm")
