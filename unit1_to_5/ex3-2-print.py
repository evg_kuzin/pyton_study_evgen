#! /usr/bin/env/ python

print("Something's rotten in the state of Denmark.") 
print("    #               -- Shakespeare")


print(("Ada Lovelace"), ("born on"), ("November 27, 1852"))


print ("math calculations:")
print ("15 / 5 = ", 15 / 5)
print ("15 * 5 = ", 15 * 5)
print ("17 - 4 = ", 17 - 4)
print ("18 + 21 = ", 18+21)
print ("27 // 7 = ", 27 // 7)