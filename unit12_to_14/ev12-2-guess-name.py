#! /usr/bin/python

name = 'gabriel'
guess = input('Guess my name: ')
count = 1

while count<3 and guess.lower() != name:
    print('wrong')
    guess = input(' guess my name: ')
    count = count +1

if guess.lower() != name:
    print('you run out of attempts')
else:
    print('you are correct! my name is ', name+'!')
