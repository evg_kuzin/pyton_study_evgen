#! /usr/bin/local/python3


import csv

data_list =[]
with open('csv.csv') as csv_file: #csv.csv is an input file with the rows of data
    file_reader = csv.reader(csv_file)
    for row in file_reader:
        data_list.append(row)


column_chart_data = [data_list[0]].   #saving lables into a variable
for row in data_list[1:]:
    num_asserts = int(row[1])
    num_failed_asserts = int(row[2])
    count = int(row[3])
    column_chart_data.append([row[0],num_asserts,num_failed_asserts,count]) #filling the chart data in a proper format


from string import Template
#here the following lines are the base for our nre html file
html_string = Template("""<html>.
<head>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {packages: ['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart () {
      var data = google.visualization.arrayToDataTable([
       $labels,
       $data
      ],
      false); // 'false' means that the first row contains labels, not data.
    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
      chart.draw(data);
  }
</script>
</head>
<body>
<div id="chart_div" style="width:800; height:600"></div>
</body>
</html>""")


chart_data_str ='' #writing into a string the same chart data starting with second row (excluding)
for row in column_chart_data[1:]:
    chart_data_str += '%s,\n'%row


completed_html = html_string.substitute(labels=column_chart_data[0], # fill in the html
data=chart_data_str)

with open('column_chart.html','w') as f:
    f.write(completed_html)

    #debug
# print('column_chart_data:')
# print(column_chart_data)
# print("**********************")
# print("chart data string:")
# print(chart_data_str)

