#! /usr/bin/python


for count in range(1,10):
    print(count)

d = list(range(20,35))
print(d)

print("")
demolist = ["life",42,"is","no","longer",317,"valid"]
for i in demolist:
    print("current item is: ",i)

print("***********************")

list =[2,4,5,11]
sum = 0
for nim in list:
    sum = sum+nim

print ("sum is: ", sum)

print("***********************")

list = [1,3,6,4,3,5,7,8,7,6,5,4]
prev = None
list.sort()
for i in list:
    if prev == i:
        print("duplicate is found: ", i)
    prev =i


print("***********************")

l = [4,5,7,8,9,1,0,7,10]
print ("l = [4,5,7,8,9,1,0,7,10]", "\t\tl:",l)
l.sort()
print("l.sort()", "\t\tl:",l)
prev=l[0]
print("prev=l[0]:","\t\tprev",prev)
del l[0]
print("del l[0]","\t\tl", l)
for item in l:
    if prev == item:
        print ("duplicate of ", prev, " is found")
    print ("if prev == item: ","\t\tprev: ", prev, "\titem: ", item)
    prev=item
    print("prev=item        ", "\t\tprev: ", prev, "\titem: ", item)
