#! /usr/bin/python

## This program runs
# First get the test # Later this will be

def get_questions():

    return [["What color is the daytime sky on a clear day? ", "blue"],
["What is the answer to life, the universe and everything? ", "42"], ["What is a three letter word for mouse trap? ", "cat"]]

def check_question(question_and_answer):
# extract the question and the answer from the list
# This function takes a list with two elements, a question and an answer.
    question = question_and_answer[0]
    answer = question_and_answer[1]
# give the question to the user
    given_answer = input(question)
# compare the user's answer to the tester's answer
    if answer == given_answer:
        print("Correct")
        return True
    else:
        print("Incorrect, correct was:", answer)
        return False
# This will run through all the questions
def run_test(questions):
    if len(questions) == 0:
        print("No questions were given.") # the return exits the function
        return
    index = 0
    right = 0
    while index < len(questions):
# Check the question
#Note that this is extracting a question
        if check_question(questions[index]):
            right = right + 1
# go to the next question
        index = index + 1
# notice the order of the computation, first
#and answer list from the list of lists.
#multiply, then divide
    print("You got", right * 100 / len(questions),\
        "% right out of", len(questions))
# now let's get the questions from the get_questions function, and
# send the returned list of lists as an argument to the run_test function.
def options():
    print("Please select an option")
    print("")
    print("a: take the test")
    print("b: view the list of questions and answers")
    print("q: quit")
i=0
inp = "x"
while inp != "q":
    inp = input("Please select your option: ")
    if inp == "a":
        run_test(get_questions())
        options()
    elif inp == "b":
        questions = get_questions()
        while i< len(get_questions()):
            print("question is:")
            print(questions[i][0])
            print("answer is")
            print(questions[i][1])
            i=i+1
        options()
    else:
        print ("select valid option")
        options()






